## Directories Tree

    
    .
    ├── IPOP_WebService
        ├── setup.sh
        ├── config.py
        ├── MongoCollector.py
        ├── InfluxCollector.py
        ├── DeploymentServer.py
        ├── DatabasePool.py
        ├── ContainerService.py
        ├── CollectorService.py
        ├── CentralVisualizerService.py
    ├── TIG_Stack_and_Sensor_Code
        ├── docker_telegraf
            ├── docker-compose.yml
            ├── telegraf
                ├── conf
                    ├── telegraf.conf
        ├── docker_centralized
            ├── docker-compose.yml
        ├── sensor_code
            ├── dht22.py
            ├── ldr.py
            ├── ldrMqtt.py
        ├── setup.sh


```
1. IPOP_WebService is the Directory for IPOP web_service.
2. TIG Stack and Sensor Code is the Directory for setting up the TIG Stack and we also attach sensor code of DHT22 and LDR.
```
