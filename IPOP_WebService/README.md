# Requirement
   
*  Ubuntu 18 ++
*  python3 library (just install setup below)

# To Install

Enter `./setup.sh`.

# To Change Database

1. vi or  nano `config.py`
2. You'll see `database field`  in `COLLECTOR_CONFIG `and `VISUALIZER_CONFIG`
3. if you would like to use `InfluxDB` edit database field to be `influx`
4. if you would like to use `MongoDB` edit database field to be `mongo`


# To run the service

Enter `sudo python3 Deployment.py` 



# Noted

Now this service is debugging version, if there are error, please feel free to
tell us.
 
Bests :)