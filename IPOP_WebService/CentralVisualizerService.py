#!/usr/bin/env python
import time, json, sys, logging
from InfluxCollector import InfluxCollector
from MongoCollector import MongoCollector
from flask import Flask, make_response, render_template, request
from datetime import datetime
class CentralVisualizerService(object):
  
    def __init__(self, config_dict):

        #TO DO, Find the way to scale below code
        if config_dict["database"] == "influx":
            self._db = InfluxCollector(config_dict)
        elif config_dict["database"] == "mongo":
            self._db = MongoCollector(config_dict)
        ########################################

    # #route('/IPOP')
    # def homepage(self):
    #     resp =  render_template('ipop_mainpage.html')
    #     return resp

    #route('/IPOP/intervals', methods=['GET'])
    def get_intervals(self):
        start_time = datetime.strptime(request.args.get('start') , "%Y-%m-%dT%H:%M:%S")
        end_time = datetime.strptime(request.args.get('end') , "%Y-%m-%dT%H:%M:%S")
        interval_numbers = []
        for docs in self._mongo_data.find({"_id": {"$gt": start_time , "$lt": end_time}}, {"_id":1}):
            interval_numbers.append(str(docs["_id"]))
        response_msg = {"IPOP" : {
                                "IntervalNumbers" : interval_numbers
                        }}
        resp = make_response(json.dumps(response_msg))
        resp.headers['Content-Type'] = "application/json"
        return resp

    

    #route('/IPOP/overlays', methods=['GET'])
    def get_overlays(self):
        resp = self._db.get_overlays()
        return resp

    #route('/IPOP/overlays/<overlayid>/nodes', methods=['GET'])
    def get_nodes_in_an_overlay(self,overlayid):
        resp = self._db.get_nodes_in_an_overlay(overlayid)
        return resp

    #route('/IPOP/overlays/<overlayid>/nodes/<nodeid>', methods=['GET'])
    def get_single_node(self,overlayid,nodeid):
        resp = self._db.get_single_node(overlayid, nodeid)
        return resp

    #route('/IPOP/overlays/<overlayid>/links', methods=['GET'])
    def get_links_in_an_overlay(self,overlayid):
        resp = self._db.get_links_in_an_overlay(overlayid)
        return resp

    #route('/IPOP/overlays/<overlayid>/nodes/<nodeid>/links', methods=['GET'])
    def get_links_for_a_node(self,overlayid,nodeid):
        resp = self._db.get_links_for_a_node(overlayid, nodeid)
        return resp
