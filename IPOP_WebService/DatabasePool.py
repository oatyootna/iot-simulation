from abc import  ABCMeta, abstractmethod

class DatabasePoolInstance(metaclass=ABCMeta):
    
    @abstractmethod
    def __init__(self):
        raise NotImplementedError("Need to implement insert_data ()")

    @abstractmethod
    def _reset_data_held(self):
        raise NotImplementedError("Need to implement insert_data ()")
    
    @abstractmethod
    def build_data(self, req, req_data, node_id):
        raise NotImplementedError("Need to implement return_built_data ()")

    @abstractmethod
    def insert_data(self):
        raise NotImplementedError("Need to implement insert_data ()")
    
    @abstractmethod
    def get_overlays(self):
        raise NotImplementedError("Need to implement get_overlays ()")
   
    @abstractmethod
    def get_single_node(self, overlay_id):
        raise NotImplementedError("Need to implement get_single_node()")
    
    @abstractmethod
    def get_nodes_in_an_overlay(self, overlay_id):
        raise NotImplementedError("Need to implement get_nodes_in_an_overlay()")
    
    @abstractmethod
    def get_links_for_a_node(self,overlayid,nodeid):
        raise NotImplementedError("Need to implement get_links_for_a_node()")
    
    @abstractmethod
    def get_links_in_an_overlay(self,overlayid):
        raise NotImplementedError("Need to implement get_links_for_a_node()")