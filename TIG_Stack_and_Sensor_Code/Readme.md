# Sensor installation requirements
   
*  Ubuntu 18 ++ (ARM Version)
*  python 2 (just install setup below)


# To Install

**Download git repo**

1. `git clone https://gitlab.com/oatyootna/iot-simulation.git`

2. `cd iot-simulation/TIG_Stack_and_Sensor_Code/`

**Setup Environments**

1. `chmod +x setup.sh`

2. `./setup.sh`

3. `cd docker_Telegraf/telegraf/conf`

4. `nano telegraf.conf`

*for more detail [Telegraf plugins](https://docs.influxdata.com/telegraf/v1.14/plugins/plugin-list/#:~:text=Telegraf%20plugins,output%2C%20aggregator%2C%20and%20processor.)

# To Run Telegraf

1. `cd ../..`

2. `sudo docker-compose up`

if you would like to run Telegraf by deamon process

`sudo docker-compose up -d`

# To Stop Telegraf

`Ctrl+C`

if you would like to stop Telegraf that run by daemon process

`sudo docker-compose down`

# To run Grafana and InfluxDB
1. `cd docker_centralize`
2. `sudo docker-compose up -d`

# To stop Grafana and InfluxDB 
`sudo docker-compose down`




