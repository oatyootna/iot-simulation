#! /bin/bash

sudo apt-get update
sudo apt-get install docker.io
sudo apt-get install docker-compose
sudo apt-get install build-essential python-dev
sudo apt-get install python-pip
sudo pip install Adafruit_DHT
sudo pip install Rpi.GPIO
sudo pip install paho-mqtt