#!/usr/bin/python
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt

GPIO.setmode(GPIO.BCM)
GPIO.setup(4,GPIO.IN)

#IP address of MQTT Broker
broker_address = "x.x.x.x"

client = mqtt.Client()
client.connect(broker_address, 1883, 61)
sensor_data = "light_sensor status="+str(GPIO.input(4)) 

client.publish("sensors", sensor_data )
client.disconnect()


