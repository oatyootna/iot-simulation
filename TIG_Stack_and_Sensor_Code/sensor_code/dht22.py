#!/usr/bin/python

import Adafruit_DHT
from datetime import datetime

sensor = Adafruit_DHT.DHT22
PIN = 8

humidity, temperature = Adafruit_DHT.read_retry(sensor, PIN)
if humidity is not None and temperature is not None:
    f = open("DHT22.log", "a")
    f.write(str(datetime.now())+ " ")
    f.write('Temp={0:0.1f} C  Humidity={1:0.1f}%'.format(temperature, humidity))
    f.close()